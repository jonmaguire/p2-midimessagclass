//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Jonny Maguire on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

/** Class for storing and manipulating MIDI Note messages*/

class MidiMessage
{
public:
    MidiMessage(); // Default Constructor
    
    //Alternative Constructor
   // MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel); //New Constructor
    
    ~MidiMessage(); //Destructor
    
    void setNoteNumber (int value); //Mutator
    
    
    int getNoteNumber() const; //Accessor (Returns note number of the message)
    
    
    float getMidiNoteInHertz() const; //Accessor (Returns the Midi note as Freq)
    
    float getFloatVelocity() const;
    
    void setChannel (int channel); // Channel Mutator
    
    int getChannel() const; // Channel Accessor
    
    
private:
    int number;
    int velocity;
    int channel;
};


#endif /* MidiMessage_hpp */
