//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Jonny Maguire on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

//MidiMessage.cpp Written by Jonny Maguire

#include "MidiMessage.hpp"
#include <cmath>
#include <iostream>

MidiMessage::MidiMessage() //Constructor
{
    number = 60;
}

MidiMessage::~MidiMessage() //Destructor
{
}

void MidiMessage::setNoteNumber(int noteValue) // Midi note number mutator
{
    number = noteValue;
}

int MidiMessage::getNoteNumber() const // Midi note number accessor
{
    return number;
}

float MidiMessage::getMidiNoteInHertz() const // Freq Accessor and conversion
{
    return 440 * pow(2, (number - 69) / 12.0);
}

float MidiMessage::getFloatVelocity() const // Velocity Accessor

{
    float velocity = getNoteNumber();
    if (velocity < 127.0)
    {
        velocity = number/127.0;
    }
    return velocity;
}

void MidiMessage::setChannel (int channelValue) // Channel Mutator
{
    channel = channelValue;
}

int MidiMessage::getChannel() const // Channel Accessor
{
    return channel;
}

