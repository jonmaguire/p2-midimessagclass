//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//
#include <cmath>
#include <iostream>
#include "MidiMessage.hpp"




    
//    float getFloatVelocity() const
//    
//    {
//        float velocity = getNoteNumber();
//        if (velocity < 127.0)
//        {
//            velocity = number/127.0;
//        } return velocity;
//    }
    
//    float getchannel() const
//    
//    {
//        float channel = getNoteNumber();
//        
//    }
    

int main()
{
   // MidiMessage note (48, 60, 5); //call to second contructor
    
    MidiMessage note;
    int noteNumberFromConsole; //Stores input from console
    
    std::cout << "Type in a number to assign it to the MidiMessage A\n";
    
    std::cin >> noteNumberFromConsole;
    note.setNoteNumber(noteNumberFromConsole);
    
    note.getMidiNoteInHertz();
    std::cout << note.getMidiNoteInHertz();
    
    std::cout << "Type in another... number to assign it to the MidiMessage A\n";
    
    std::cin >> noteNumberFromConsole;
    note.setNoteNumber(noteNumberFromConsole);
    
    note.getMidiNoteInHertz();
    std::cout << "\n" << note.getMidiNoteInHertz(); // print freq
    
    std::cout << note.getChannel(); // print channel value
    
    return 0;
}

//class Array
//{
//public:
//    Array()
//    {
//        size = 0;
//        array = nullptr;
//    }
//    
//    void add (float newValue)
//    {
//        //Allocate new array size (size + 1)
//        float* tempArray = new float [size + 1];
//        //Copy contents of the current array
//        //add the new value to the end of the new array
//        //delete old array
//        //make our old array
//        //make our array pointer point to the new array
//        // increase size by one
//    }
//};
